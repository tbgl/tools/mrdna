## TODO: make module this package conform to a single style for input/output

def read_cadnano_legacy(json_file, sequence=None, fill_sequence='T', **model_parameters):
    from .cadnano_segments import read_json_file
    from .cadnano_segments import read_model as model_from_cadnano_json

    data = read_json_file(json_file)
    return model_from_cadnano_json(data, sequence, fill_sequence, **model_parameters)

def read_cadnano(json_file,sequence=None,**model_parameters):
    from .segmentmodel_from_cadnano import mrdna_model_from_cadnano
    
    return mrdna_model_from_cadnano(json_file,seq=sequence,**model_parameters)
def read_scadnano(sc_file, **model_parameters):
    from .segmentmodel_from_scadnano import mrdna_model_from_scadnano
    return mrdna_model_from_scadnano(sc_file, **model_parameters)

def read_vhelix(maya_file, **model_parameters):
    from .polygon_mesh import parse_maya_file, convert_maya_bases_to_segment_model

    maya_bases = parse_maya_file(maya_file)
    model = convert_maya_bases_to_segment_model( maya_bases, **model_parameters )
    return model

def read_list(infile,**model_parameters):
    import numpy as np
    try:
        data = np.loadtxt(infile)
    except:
        data = np.loadtxt(infile,skiprows=1) # strip header
    coords = data[:,:3]*10
    bp = data[:,3]
    stack = data[:,4]
    three_prime = data[:,5]

    from .segmentmodel_from_lists import model_from_basepair_stack_3prime
    return model_from_basepair_stack_3prime(coords, bp, stack, three_prime)

def read_pandas(df,coordinate_col="r",bp_col="bp",stack_col="stack",three_prime_col="threeprime",
                        seq_col="seq",orientation_col="orientation"):
    from .segmentmodel_from_lists import model_from_pandas
    return model_from_pandas(df,coordinate_col,bp_col,stack_col,three_prime_col,
                        seq_col,orientation_col)

def read_atomic_pdb(pdb_file, **model_parameters):
    from .segmentmodel_from_pdb import SegmentModelFromPdb
    return SegmentModelFromPdb(pdb_file)

def read_oxdna(coordinate_file, topology_file, virt2nuc=None, **model_parameters):
    from .segmentmodel_from_oxdna import mrdna_model_from_oxdna
    return mrdna_model_from_oxdna(coordinate_file, topology_file,virt2nuc, **model_parameters)
