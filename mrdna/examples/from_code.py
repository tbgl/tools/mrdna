import numpy as np
from mrdna import SegmentModel, SingleStrandedSegment, DoubleStrandedSegment
from mrdna.simulate import multiresolution_simulation as simulate

"""
Example using segmentmodel API to construct a model

"""

if __name__ == "__main__":
    
    """ First create the individual helices and strands """
    seg1 = DoubleStrandedSegment("strand", num_bp = 46)

    seg2 = SingleStrandedSegment("strand", 
                                 start_position = seg1.end_position + np.array((5,0,5)),
                                 num_nt = 12)

    seg3 = SingleStrandedSegment("strand", 
                                 start_position = seg1.start_position + np.array((-5,0,-5)),
                                 end_position = seg1.end_position + np.array((-5,0,5)),
                                 num_nt = 128)

    """ Each "Segment" will has a "start" and "end"
    - start3 and end5 are an attribute for all Segments
    - start5 and end3 are an attribute for only DoubleStrandedSegment
    The 3/5 refers to 3' or 5' end of a DNA strand
    """

    seg1.start3
    seg1.start5
    seg1.end3
    seg1.end5

    """ Add intrahelical connections """
    seg1.connect_end3(seg2)     # equivalent for ssDNA to: seg1.connect_end3(seg2.end5)
    seg1.connect_end5(seg3)
    seg1.connect_start3(seg3)

    """ Add crossover connections """
    ...


    """ Create a model """
    model = SegmentModel( [seg1, seg2, seg3], 
                          local_twist = True,
                          max_basepairs_per_bead = 5,
                          max_nucleotides_per_bead = 5,
                          dimensions=(5000,5000,5000),
                      )
    
    """ Simulate """
    simulate( model,
              output_name = "strand-test", 
              coarse_steps = 1e7,
              fine_steps = 1e7,
              gpu=1 )
