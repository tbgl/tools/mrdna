from .arbdmodel.coords import *

from sys import stderr
stderr.write("WARNING: the module 'mrdna.coords' has been deprecated. Please change your scripts to use 'mrdna.arbdmodel.coords'\n")
