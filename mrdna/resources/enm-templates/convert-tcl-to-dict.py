import json
from pyparsing import nestedExpr, Word, alphanums


def readTcl(filename):
    enm = dict()
    with open(filename) as fh:
        line = fh.readline()
        # parser = nestedExpr('{','}',content=Word(alphanums,alphanums+" "+"'"+"."+","))
        parser = nestedExpr('{','}')
        data = parser.parseString("{"+line+"}").asList()[0][-1]
        # chunks = parser.parseString("{hi how {hello {hownow}}}").asList()[0]
        # print(len(chunks[-1]))
        # print(chunks[-1])
        
        for key,val in zip(data[::2],data[1::2]):
            enm[key] = val
    return enm

def convert(fileIn, fileOut):
    with open(fileOut, 'w') as fh:
        json.dump(readTcl(fileIn), fh)

convert( "template-exb.tcl", "../enm-template-honeycomb.json" )
convert( "template-exb-square.tcl", "../enm-template-square.json" )
convert( "corrections.tcl", "../enm-corrections-honecomb.json" )
