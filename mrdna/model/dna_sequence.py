from .. import get_resource_path

_m13_path = get_resource_path("cadnano2pdb.seq.m13mp18.dat")
def read_sequence_file(sequenceFile=_m13_path):
    seq = []
    with open(sequenceFile) as ch:
        for l in ch:
            l = l.strip().replace(" ", "")
            if len(l) == 0 or l[0] in (";","#"): continue
            seq.extend([c.upper() for c in l])
    return list(seq)

m13 = read_sequence_file()

