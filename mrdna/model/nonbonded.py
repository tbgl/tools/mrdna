from ..arbdmodel.nonbonded import *

from sys import stderr
stderr.write("WARNING: the module 'mrdna.model.nonbonded' has been deprecated. Please change your scripts to use 'mrdna.arbdmodel.nonbonded'\n")
