## Set up loggers
import logging
def _get_username():
    import sys
    try:
        return sys.environ['USER']
    except:
        return None

logging.basicConfig(format='%(asctime)s %(name)s: %(levelname)s: %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
_ch = logging.StreamHandler()
_ch.setFormatter(logging.Formatter('%(asctime)s %(name)s: %(levelname)s: %(message)s'))
logger.addHandler(_ch)
logger.propagate = False

devlogger = logging.getLogger(__name__+'.dev')
# devlogger.setLevel(logging.DEBUG)
if _get_username() not in ('cmaffeo2',):
    devlogger.addHandler(logging.NullHandler())

## Import resources
from pathlib import Path

from .version import get_version
__version__ = get_version() 


_RESOURCE_DIR = Path(__file__).parent / 'resources'
def get_resource_path(relative_path):
    return _RESOURCE_DIR / relative_path

## Import useful messsages
from .arbdmodel.coords import readArbdCoords
from .segmentmodel import SegmentModel, SingleStrandedSegment, DoubleStrandedSegment
from .simulate import multiresolution_simulation
from .model.dna_sequence import read_sequence_file

from .reporting import report

report("mrdna.__init__")

"""
  _____ ___|    \|   | |  _  |
 |     |  _|  |  | | | |     |
 |_|_|_|_| |____/|_|___|__|__|
"""

print("""             _
 _____ ___ _| |___ ___
|     |  _| . |   | .'|
|_|_|_|_| |___|_|_|__,|  v{}  
it/its
""".format(__version__))
