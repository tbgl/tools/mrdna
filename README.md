# Multi-Resolution DNA simulations
A Python3 package that makes it easy to run simulations of DNA nanostructures.
The multi-resolution approach provides the benefits of coarse-grained modeling, 
while resulting in a high-resolution structure suitable for atomistic simulations.
Alternatively, the package can be used to initialize simulations using the oxDNA model.

## Dependencies
* Linux operating system
* g++ >= 4.8
* [CUDA toolkit](https://developer.nvidia.com/cuda-toolkit) >= 6
* [ARBD](http://bionano.physics.illinois.edu/arbd) simulation engine
* Python >= 3.5 *(some users report problems installing mdanalysis; miniconda or conda are recommended)*
  * numpy >= 1.14
  * scipy >= 1.1
  * mdanalysis >= 0.18 
  * cadnano >= 2.5.2.1
  * appdirs >= 1.4
  * pandas >= 1.0
  * scadnano >= 0.1 (optional)
  * [oxDNA](https://dna.physics.ox.ac.uk/index.php/Main_Page) (optional)

## Installation
First make sure you have the cuda-toolkit installed.
First download ARBD through a web browser.
The following (untested) script can then be modified and used to install mrdna.
Please report issues to [Chris Maffeo](mailto:cmaffeo2@illinois.edu).
```bash

# Customize the following
BUILD_DIR=/path/to/build-dir
export CUDA_PATH=/path/to/cuda-toolkit

# Unpack and build arbd
cd $BUILD_DIR
mv ~/Downloads/arbd*.tar.gz .
tar -xzf arbd*.tar.gz
cd arbd
cmake -S . -B build && (
  cd build
  make -j
)
cd ../../

## Possibly setup python dependencies (you may want to set up a virtual environement) using commands below
# conda install "numpy>=1.14" "scipy>=1.1" "appdirs>=1.4"
# conda install -c conda-forge "mdanalysis>=0.18" "pandas>=1.0"
# pip3 install "cadnano>=2.5.2.1" "scadnano>=0.1" 

git clone https://gitlab.engr.illinois.edu/tbgl/tools/mrdna
cd mrdna
pip3 install .
cd ..

# Finally update your bashrc so that your shell can find the ARBD binary
echo "PATH=$BUILD_DIR/arbd/src:\$PATH" >> ~/.bashrc
source ~/.bashrc
```
